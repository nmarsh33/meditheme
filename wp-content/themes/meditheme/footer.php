<?php
/**
 * The template for displaying the footer.
 */
?>

<section class="footer">
	<div class="top-footer container"> <!--begin top footer container-->
		<div class="row">
			<div class="about-footer col-xs-12 col-sm-6 col-md-3 col-lg-3"> <!--begin about footer-->
				<h4 class="text-uppercase">About Acenta</h4>
				<p>ACENTA offers the most comprehensive ENT, Allergy, Hearing Aid, and Hearing & Balance services in the
					Northwest Arkansas (Including Fort Smith, Van Buren, Alma, Fayetteville, Rogers Arkansas) and
					Eastern Oklahoma region.
				<p>
				<p>We offer appointment times that are as flexible as possible to accommodate
					our patients’ busy schedules.

				</p>

			</div> <!--end about footer-->
			<div class="contact-footer col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<h4 class="text-uppercase">Contact Us</h4>
				<p><i class="fa fa-map-marker" aria-hidden="true"></i><strong>Address:</strong> 7805 Phoenix Ave.
					Fort Smith, AR 72903</p>
				<p><i class="fa fa-mobile" aria-hidden="true"></i><strong>Phone: +</strong> 479-242-4220</p>
				<p><i class="fa fa-fax" aria-hidden="true"></i><strong>Fax:</strong> 479-242-4221</p>
				<!--				<p><i class="fa fa-envelope-o" aria-hidden="true"></i><strong>Email:</strong> info@sana.com</p>
				-->            </div>

			<div class="news-footer col-xs-12 col-sm-6 col-md-3 col-lg-3"> <!--begin recent news-->
				<h4 class="text-uppercase">Recent News</h4>


				<?php

				// The Query

				$args = array(
					'post_type' => 'post',
					'orderby'   => 'date',
					'order'     => 'DESC'
				);

				$the_query = new WP_Query( $args );

				// The Loop
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post(); ?>

						<div class="blog-item"> <!--blog item container-->


							<a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail( array( 42, 42 ) ); ?></a>

							<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>

							<p><?php get_the_date( 'l, d' ); ?></p>


						</div> <!--end blog item container-->

					<?php } ?>


					<?php

					/* Restore original Post Data */
					wp_reset_postdata();
				} else {
					// no posts found
				}
				?>


			</div> <!--end recent news-->

			<div class="services-footer col-xs-12 col-sm-6 col-md-3 col-lg-2"> <!--begin category menu-->
				<h4 class="text-uppercase">Services</h4>
				<?php wp_nav_menu(
					array(
						'theme_location' => 'services',
						'container'      => false,
						'fallback_cb'    => '',
						'depth'          => 1
					)
				); ?>
			</div> <!--end category menu-->
		</div>
	</div> <!-- end top footer container -->
	<section class="bottom-container">
		<div class="bottom-footer container"> <!-- begin bottom footer -->
			<div class="row flex-center">
				<div class="col-sm-12 col-md-12 col-lg-9"> <!-- begin menu -->

					<?php wp_nav_menu(
						array(
							'theme_location' => 'footer',
							'container'      => false,
							'fallback_cb'    => '',
							'depth'          => 1
						)
					); ?>

				</div> <!--end menu-->

				<div class="text-xs-center col-xs-12 col-sm-12 col-md-12 col-lg-3">
					<p class="copyright text-xs-center">©<?php echo date( 'Y' ); ?> <?php echo get_bloginfo(); ?>. All Rights
						Reserved</p>
				</div>

			</div>
		</div> <!--end bottom footer-->
	</section>
</section>

</body>
</html>

<?php wp_footer(); ?>


