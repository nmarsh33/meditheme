<?php
/**
 * Index File
 */

get_header();

?>

<?php hero(); ?>

<div class="container main">

	<div class="row">

		<div class="col-lg-12">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile;
		else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>

		</div>

	</div>

</div>

<?php get_footer(); ?>
