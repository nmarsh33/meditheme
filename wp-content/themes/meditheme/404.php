<?php
/**
 * The template for displaying 404 pages (Not Found)
 */
get_header(); ?>

<div class="main container">

	<div class="row">

		<p>Sorry, the page you are looking for, seems to be missing.  Click <a href="/">here</a> to go back to the homepage.</p>

	</div>

</div>

<?php get_footer(); ?>